from random import randint

year_min, year_max = 1924, 2004
months = [
    ""
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]

name = input("Hi! What is your name?")


for guess in range(5):
    month_guess = randint(1, 12)
    year_guess = randint(year_min, year_max)
    print(name, "were you born in", months[month_guess], "/", year_guess, "?")
    answer = input("yes, earlier, or later? ")
    if answer == "yes":
        print("I knew it!")
        break
    elif answer == "earlier" and guess < 4:
        print("Drat! Lemme try again!")
        year_max = year_guess
    elif answer == "later" and guess < 4:
        print("Drat! Let me try again!")
        year_min = year_guess
    elif guess == 4:
        print("I have other things to do. Goodbye.")
